import cPickle as pickle

class Pickler(object):

    def pakuj(self, obj):
        self.pick = pickle.dumps(obj)
        return self.pick

    def rozpakuj(self, data):
        self.data_loaded = pickle.loads(data)
        return self.data_loaded

    def is_pickle(self, data):
        try:
            pickle.loads(data)
            return True
        except:
            return False