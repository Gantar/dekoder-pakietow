# -*- coding: utf-8 -*-
import socket

class WyslijNaSerwer(object):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(("127.0.01", 2094))

    def wyslij(self, data):
        self.sock.sendall(data + "\r\n")


    def odbierz(self):
        data = self.recv_all_until(self.sock, "\r\n")
        return data


    def recv_all_until(self, sockfd, crlf):
        data = ""
        while not data.endswith(crlf):
            data = data + sockfd.recv(1)
        return data
