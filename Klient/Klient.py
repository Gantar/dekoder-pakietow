# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore

import Start
import Wyslij


class Klient(QtGui.QWizard):
    NUM_PAGES = 2
    (Pierwsza, Druga) = range(NUM_PAGES)
    def __init__(self, parent = None):
        super(Klient, self).__init__(parent)
        self.setGeometry(0, 0, 1024, 800)
        self.setWindowTitle("Dekoder pakietow")
        self.setFixedSize(self.size())
        self.label1 = QtGui.QLabel(self)
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.CancelButton, "Anuluj")
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.FinishButton, "Koniec")
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.BackButton, QtCore.QString.fromUtf8("Powrót"))
        QtGui.QWizard.setButtonText(self, QtGui.QWizard.NextButton, "Dalej")

        self.setPage(self.Pierwsza, Start.Start(self))
        self.setPage(self.Druga, Wyslij.Wyslij(self))

