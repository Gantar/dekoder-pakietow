# -*- coding: utf-8 -*-
from PyQt4 import QtGui
import sys

import Klient


def main():


    app = QtGui.QApplication(sys.argv)
    wiz = Klient.Klient()
    wiz.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()