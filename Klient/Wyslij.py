# -*- coding: utf-8 -*-
import Klient, ObiektSerwerowy, Pickler, WyslijNaSerwer

class Wyslij(Klient.QtGui.QWizardPage):

    def __init__(self, parent=None):
        super(Wyslij, self).__init__(parent)

        ##ZMIENNE

        self.Pakiet = ""
        self.os = ObiektSerwerowy.ObiektSerwerowy()
        self.pickler = Pickler.Pickler()
        #self.wns = WyslijNaSerwer.WyslijNaSerwer()
        #####

        self.l1 = Klient.QtGui.QLabel(self)
        self.l1.setText(
            Klient.QtCore.QString.fromUtf8(
'''Spośród 10 dostępnych rodzajów pakietów proszę wybrać rodzaj pakietu,
oraz kodowanie tego pakietu'''))
        self.l1.setFont(Klient.QtGui.QFont("Helvetica", 20))
        self.l1.move(50, 50)

        self.CB = Klient.QtGui.QComboBox(self)
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("DHCPv6"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("TCP"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("UDP"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("IP"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("Nat-PMP"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("ICMP"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("ARP"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("DNS"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("STUN"))
        self.CB.addItem(Klient.QtCore.QString.fromUtf8("CAT-TP"))
        self.CB.move(100, 150)

        self.CB_typ_danych = Klient.QtGui.QComboBox(self)
        self.CB_typ_danych.addItem(Klient.QtCore.QString.fromUtf8("Binary"))
        self.CB_typ_danych.addItem(Klient.QtCore.QString.fromUtf8("HEX"))
        self.CB_typ_danych.move(250, 150)


        self.l1 = Klient.QtGui.QLabel(self)
        self.l1.setText(Klient.QtCore.QString.fromUtf8("Proszę wybrać pakiet do odkodowania"))
        self.l1.setFont(Klient.QtGui.QFont("Helvetica", 20))
        self.l1.move(50, 200)

        self.RB_zpliku = Klient.QtGui.QRadioButton(self)
        self.RB_zpliku.toggled.connect(self.RB_check_zpliku)
        self.RB_zpliku.move(20, 200)

        self.pathLabel = Klient.QtGui.QLineEdit(self)
        self.pathLabel.setEnabled(False)
        self.pathLabel.setFixedSize(250, 30)
        self.pathLabel.move(100,250)

        self.wybierzPakietBtn = Klient.QtGui.QPushButton(self)
        self.wybierzPakietBtn.setText(Klient.QtCore.QString.fromUtf8("Przeglądaj..."))
        self.wybierzPakietBtn.setEnabled(False)
        self.wybierzPakietBtn.setFont(Klient.QtGui.QFont("Helvetica", 12))
        self.connect(self.wybierzPakietBtn, Klient.QtCore.SIGNAL("clicked()"), self.wyszukajPlik)
        self.wybierzPakietBtn.move(450, 250)

        self.l1 = Klient.QtGui.QLabel(self)
        self.l1.setText(Klient.QtCore.QString.fromUtf8("Lub wpisać pakiet do wysłania ręcznie"))
        self.l1.setFont(Klient.QtGui.QFont("Helvetica", 20))
        self.l1.move(50, 300)

        self.RB_recznie = Klient.QtGui.QRadioButton(self)
        self.RB_recznie.move(20, 300)
        self.RB_recznie.toggled.connect(self.RB_check_recznie)

        self.pathLabel2 = Klient.QtGui.QLineEdit(self)
        self.pathLabel2.setEnabled(False)
        self.pathLabel2.setFixedSize(250, 30)
        self.pathLabel2.move(100, 350)


        self.sendBtn = Klient.QtGui.QPushButton(self)
        self.sendBtn.setText(Klient.QtCore.QString.fromUtf8("Wyślij"))
        self.connect(self.sendBtn, Klient.QtCore.SIGNAL("clicked()"), self.wyslij)
        self.sendBtn.move(500, 500)
        self.sendBtn.setEnabled(False)








    def RB_check_recznie(self):
        self.wybierzPakietBtn.setEnabled(False)
        self.pathLabel2.setEnabled(True)
        self.pathLabel.setText("")
        self.sendBtn.setEnabled(True)


    def RB_check_zpliku(self):
        self.pathLabel2.setText("")
        self.pathLabel2.setEnabled(False)
        self.wybierzPakietBtn.setEnabled(True)
        self.sendBtn.setEnabled(True)






    def wyszukajPlik(self):
        sciezka = Klient.QtGui.QFileDialog(self)
        sciezka.setFileMode(Klient.QtGui.QFileDialog.AnyFile)
        sciezka.setFilter("Text files (*.txt)")
        filenames = Klient.QtCore.QStringList()

        if sciezka.exec_():
            filenames = sciezka.selectedFiles()
            self.pathLabel.setText(filenames[0])
            f = open(filenames[0], 'r')

            with f:
                data = f.read()
                self.Pakiet = data

    def przygotujObiekt(self, name):
        self.os.typ_danych = str(self.CB_typ_danych.currentText())
        self.os.protokol = str(self.CB.currentText())
        self.os.pakiet = name.text()
        self.os.pakiet = self.usun_spacje(self.os.pakiet)
        self.os.pakiet = str(self.os.pakiet)

    def wyslij(self):
        if self.RB_recznie.isChecked():
            self.Pakiet = self.pathLabel2.text()
            self.przygotujObiekt(self.pathLabel2)
            print "Wpisany recznie: " + self.Pakiet
        if self.RB_zpliku.isChecked():
            self.os.typ_danych = str(self.CB_typ_danych.currentText())
            self.os.protokol = str(self.CB.currentText())
            self.os.pakiet = self.usun_spacje(self.Pakiet)
            self.os.pakiet =  str(self.os.pakiet)
            print "Wybrany z pliku: " + self.Pakiet

        self.os.wyswietlPakiet()
        self.doWyslania = self.pickler.pakuj(self.os) ##Obiekt gotowy do wyslania
        wns = WyslijNaSerwer.WyslijNaSerwer()
        wns.wyslij(self.doWyslania)
        print "Wysylam Pakiet"

        msg = str(wns.odbierz())
        print "ODBEBRALEM"
        self.showMessage(msg)


    def usun_spacje(self, data):
        data = str(data)
        data = data.replace("\n", "").replace("\r", "")
        data = data.replace(" ", "")
        return data


    def showMessage(self, answer):
        self.msg = Klient.QtGui.QMessageBox(self)
        self.msg.setIcon(Klient.QtGui.QMessageBox.Information)
        self.msg.setStandardButtons(Klient.QtGui.QMessageBox.Ok)
        self.msg.setText(Klient.QtCore.QString.fromUtf8(answer))
        self.result = self.msg.exec_()