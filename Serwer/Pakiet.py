import ObiektSerwerowy, Logger
import UDP, DHCPv6, TCP, ARP, STUN, IP, ICMP, DNS, Nat_PMP, CAT_TP

class Pakiet(object):
    def __init__(self):
       # self.logger = Logger.Logger()
        self.obs = ObiektSerwerowy.ObiektSerwerowy()
        self.udp = UDP.UDP()
        self.dhcpv6 = DHCPv6.DHCPv6()
        self.tcp = TCP.TCP()
        self.arp = ARP.ARP()
        self.stun = STUN.STUN()
        self.ip = IP.IP()
        self.icmp = ICMP.ICMP()
        self.dns = DNS.DNS()
        self.nat_pmp = Nat_PMP.Nat_PMP()
        self.cat_tp = CAT_TP.CAT_TP()
        self.msg = ""




    def sprawdz_kodowanie(self, data):
        self.sprawdz_pakiet(data)


    def sprawdz_pakiet(self, data):
        if(data.protokol == "UDP"):
            if(data.typ_danych == "HEX"):
                self.udp.dlaHex(data.pakiet)
            elif(data.typ_danych == "Binary"):
                self.udp.dlaBinary(data.pakiet)
            self.udp.wyswietl()
            self.msg = self.udp.WIAD
        elif(data.protokol == "DHCPv6"):
            if (data.typ_danych == "HEX"):
                self.dhcpv6.dlaHex(data.pakiet)
                self.dhcpv6.wyswietl()
            elif (data.typ_danych == "Binary"):
                self.dhcpv6.dlaBinary(data.pakiet)
                self.dhcpv6.wyswietl()
            self.msg = self.dhcpv6.WIAD
        elif (data.protokol == "TCP"):
            if (data.typ_danych == "HEX"):
                self.tcp.dlaHex(data.pakiet)
            elif (data.typ_danych == "Binary"):
                self.tcp.dlaBinary(data.pakiet)
            self.msg = self.tcp.WIAD
        elif (data.protokol == "ARP"):
            if (data.typ_danych == "HEX"):
                self.arp.dlaHex(data.pakiet)
                self.arp.wyswietl()
            elif (data.typ_danych == "Binary"):
                self.arp.dlaBinary(data.pakiet)
                self.arp.wyswietl()
            self.msg = self.arp.WIAD
        elif (data.protokol == "STUN"):
            if (data.typ_danych == "HEX"):
                self.stun.dlaHex(data.pakiet)
            elif (data.typ_danych == "Binary"):
                self.stun.dlaBinary(data.pakiet)
            self.msg = self.stun.WIAD
        elif (data.protokol == "IP"):
            if (data.typ_danych == "HEX"):
                self.ip.dlaHex(data.pakiet)
            elif (data.typ_danych == "Binary"):
                self.ip.dlaBinary(data.pakiet)
            self.msg = self.ip.WIAD
        elif (data.protokol == "ICMP"):
            if (data.typ_danych == "HEX"):
                self.icmp.dlaHex(data.pakiet)
            elif (data.typ_danych == "Binary"):
                self.icmp.dlaBinary(data.pakiet)
            self.msg = self.icmp.WIAD
        elif (data.protokol == "DNS"):
            if (data.typ_danych == "HEX"):
                self.dns.dlaHex(data.pakiet)
            elif (data.typ_danych == "Binary"):
                self.dns.dlaBinary(data.pakiet)
            self.msg = self.dns.WIAD
        elif (data.protokol == "Nat-PMP"):
            if (data.typ_danych == "HEX"):
                self.nat_pmp.dlaHex(data.pakiet)
            elif (data.typ_danych == "Binary"):
                self.nat_pmp.dlaBinary(data.pakiet)
            self.msg = self.nat_pmp.WIAD
        elif (data.protokol == "CAT-TP"):
            if (data.typ_danych == "HEX"):
                self.cat_tp.dlaHex(data.pakiet)
                self.cat_tp.wyswietl()
            elif (data.typ_danych == "Binary"):
                self.cat_tp.dlaBinary(data.pakiet)
                self.cat_tp.wyswietl()
            self.msg = self.cat_tp.WIAD
        #self.logger.logger.info("Rozkodowany pakiet uzytkownika: " + self.msg)



