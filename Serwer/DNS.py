# -*- coding: utf-8 -*-
import Konwerter

class DNS(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.transactionID = ""
        self.flags = ""
        self.queries = ""
        self.WIAD = ""

    def dlaHex(self, pakiet):
        self.transactionID = pakiet[:4]
        self.flags = pakiet[4:8]
        self.queries = pakiet[24:48]

        #self.transactionID = self.konwerter.HEX_to_ascii(self.transactionID)
        #self.flags = self.konwerter.HEX_to_ascii(self.flags)
        self.queries = self.konwerter.HEX_to_ascii(self.queries)

        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.transactionID = pakiet[:16]
        self.flags = pakiet[16:32]
        self.queries = pakiet[96:192]

        self.transactionID = self.konwerter.bintohex2(self.transactionID)
        print(str(self.transactionID))
        self.flags = self.konwerter.bintohex2(self.flags)
        print(str(self.flags))
        self.queries = self.konwerter.BIN_to_ascii(self.queries)

        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "transactionID: " + str(self.transactionID) + " flags: " + str(self.flags) + " queries: " + str(self.queries)

    def wyswietl(self):
        print self.WIAD

