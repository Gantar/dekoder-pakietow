import Konwerter

class IP(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.wersja = ""
        self.dlugosc_naglowka = ""
        self.typ_uslug = ""
        self.calkowita_dlugosc_pakietu = ""
        self.WIAD = ""


    def dlaHex(self, pakiet):
        self.wersja = pakiet[0]
        self.dlugosc_naglowka = pakiet[1]
        self.typ_uslug = pakiet[2:4]
        self.calkowita_dlugosc_pakietu = pakiet[4:8]

        self.wersja = self.konwerter.HEX_to_dec(self.wersja)
        self.dlugosc_naglowka = self.konwerter.HEX_to_dec(self.dlugosc_naglowka )
        self.typ_uslug = self.konwerter.HEX_to_dec(self.typ_uslug)
        self.calkowita_dlugosc_pakietu = self.konwerter.HEX_to_dec(self.calkowita_dlugosc_pakietu)

        if int(self.typ_uslug) >= 128:
            self.typ_uslug = "Datagram superblyskawiczny"
        elif int(self.typ_uslug) >= 96:
            self.typ_uslug = "Datagram blyskawiczny"
        elif int(self.typ_uslug) >= 64:
            self.typ_uslug = "Datagram natychmiastowy"
        elif int(self.typ_uslug) >= 32:
            self.typ_uslug = "Datagram priorytetowy"
        elif int(self.typ_uslug) >= 0:
            self.typ_uslug = "Datagram zwykly"
        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.wersja = pakiet[:4]
        self.dlugosc_naglowka = pakiet[4:8]
        self.typ_uslug = pakiet[8:16]
        self.calkowita_dlugosc_pakietu = pakiet[16:32]

        self.wersja = self.konwerter.BIN_to_dec(self.wersja)
        self.dlugosc_naglowka = self.konwerter.BIN_to_dec(self.dlugosc_naglowka )
        self.typ_uslug = self.konwerter.BIN_to_dec(self.typ_uslug)
        self.calkowita_dlugosc_pakietu = self.konwerter.BIN_to_dec(self.calkowita_dlugosc_pakietu)


        if int(self.typ_uslug) >= 128:
            self.typ_uslug = "Datagram superblyskawiczny"
        elif int(self.typ_uslug) >= 96:
            self.typ_uslug = "Datagram blyskawiczny"
        elif int(self.typ_uslug) >= 64:
            self.typ_uslug = "Datagram natychmiastowy"
        elif int(self.typ_uslug) >= 32:
            self.typ_uslug = "Datagram priorytetowy"
        elif int(self.typ_uslug) >= 0:
            self.typ_uslug = "Datagram zwykly"

        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "wersja: " + str(self.wersja) + " dlugosc nagl: " + str(self.dlugosc_naglowka) + " typ_uslug: " + str(
        self.typ_uslug) + " Calkowita dl pakietu: " + str(self.calkowita_dlugosc_pakietu)

    def wyswietl(self):
        print self.WIAD