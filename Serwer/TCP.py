import Konwerter
class TCP(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.port_zrodlowy = ""
        self.port_docelowy = ""
        self.windows_size_val = ""

    def dlaHex(self, pakiet):
        self.port_zrodlowy = pakiet[:4]
        self.port_docelowy = pakiet[4:8]
        self.windows_size_val= pakiet[28:32]



        self.port_zrodlowy = self.konwerter.HEX_to_dec(self.port_zrodlowy)
        self.port_docelowy = self.konwerter.HEX_to_dec(self.port_docelowy)
        self.windows_size_val = self.konwerter.HEX_to_dec(self.windows_size_val)

        self.createWIAD()


    def dlaBinary(self, pakiet):
        self.port_zrodlowy = pakiet[:16]
        self.port_docelowy = pakiet[16:32]
        self.windows_size_val = pakiet[112:128]



        self.port_zrodlowy = self.konwerter.BIN_to_dec(self.port_zrodlowy)
        self.port_docelowy = self.konwerter.BIN_to_dec(self.port_docelowy)
        self.windows_size_val = self.konwerter.BIN_to_dec(self.windows_size_val)

        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "port zrodlowy: " + str(self.port_zrodlowy) + " port docelowy: " + str(self.port_docelowy) + " windows size value: " + str(self.windows_size_val)

    def wyswietl(self):
        print self.WIAD

