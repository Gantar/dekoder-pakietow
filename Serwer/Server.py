# -*- coding: utf-8 -*-
import socket, Watek, Logger

class Server(object):
    def __init__(self):

        self.clients = []
        self.logger = Logger.Logger()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_addres = ("127.0.01", 2094)
        self.sock.bind(self.server_addres)
        self.request = ""
        print 'Starting up TCP server on %s port %s' % self.server_addres
        self.logger.logger.info("Serwer TCP wystartowal na %s", self.server_addres)
        while True:
            self.sock.listen(1)
            self.connection, self.client_address = self.sock.accept()
            c = Watek.Watek(self.connection, self.client_address)
            c.start()
            self.clients.append(c)






def main():
    server = Server()

main()
