import binascii
import string
class Konwerter(object):

    def bintohex2(self, s):
        return ''.join(["%x" % string.atoi(bin, 2) for bin in s.split()])
    def BIN_to_hex(self, val):
        val = str(val)
        return binascii.hexlify(val)

    def HEX_to_ascii(self, val):
        return val.decode('hex')

    def HEX_to_dec(self, val):
        return int(val, 16)

    def BIN_to_dec(self, val):
        return int(val, 2)

    def BIN_to_ascii(self, val):
        val = int(str(val), 2)
        return binascii.unhexlify('%x' % val)

    def usun_spacje(self, data):
        data = str(data)
        data = data.replace("\n", "").replace("\r", "")
        data = data.replace(" ", "")
        return data