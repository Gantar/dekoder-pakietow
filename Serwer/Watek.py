# -*- coding: utf-8 -*-
import threading, Pickler, ObiektSerwerowy, Logger, Pakiet

class Watek(threading.Thread):
    def __init__(self, connection, client_address):
        threading.Thread.__init__(self)
        self.client_address = client_address
        self.connection = connection

        # ZMIENNE
        self.pickler = Pickler.Pickler()
        self.data_loaded = ObiektSerwerowy.ObiektSerwerowy()
        self.logger = Logger.Logger()
        self.pakiet = Pakiet.Pakiet()

    def run(self):
        print "Client %s connected ..." % str(self.client_address)
        self.logger.logger.info("uzytkownik %s zalogowal sie", self.client_address)
        try:
            self.data = self.recv_all_until(self.connection, "\r\n")
            if self.data:
                if self.pickler.is_pickle(self.data):
                    self.logger.logger.info("Przyslal obiekt zserializowany")

                    self.data_loaded = self.pickler.rozpakuj(self.data)
                    self.logger.logger.info(" Protokol pakietu : "  + self.data_loaded.protokol + " Typ danych: " + self.data_loaded.typ_danych +
                                           " Pakiet : " + self.data_loaded.pakiet)
                    try:
                        self.pakiet.sprawdz_kodowanie(self.data_loaded)
                        self.logger.logger.info("Rozkodowany pakiet: " + self.pakiet.msg)
                        msgg = self.pakiet.msg + "\r\n"
                        self.connection.sendall(msgg)
                    except :
                        self.connection.sendall("Wystapil problem z pakietem, sprobuj innego kodowania lub wpisz pakiet poprawnie" + "\r\n")
                        self.logger.logger.info("Wystapil problem z rozkodowaniem pakietu")








        finally:
            self.connection.close()

    def recv_all_until(self, sockfd, crlf):
        data = ""
        while not data.endswith(crlf):
            data = data + sockfd.recv(1)
        return data




