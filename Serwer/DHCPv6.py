import Konwerter

class DHCPv6(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.transaction_ID = ""
        self.link_layer = ""
        self.clientFODN = ""
        self.WIAD = ""

    def dlaHex(self, pakiet):
        self.transaction_ID = pakiet[2:8]
        self.link_layer = pakiet[46:56]
        self.clientFODN = pakiet[100:124]

        #self.transaction_ID = self.konwerter.HEX_to_dec(self.transaction_ID)
       # self.link_layer = self.konwerter.HEX_to_ascii(self.link_layer)
        self.clientFODN = self.konwerter.HEX_to_ascii(self.clientFODN)
        self.createWIAD()


    def dlaBinary(self, pakiet):
         self.transaction_ID = pakiet[8:32]
         self.link_layer = pakiet[184:224]
         self.clientFODN = pakiet[400:496]

         self.transaction_ID = self.konwerter.bintohex2(self.transaction_ID)
         self.link_layer = self.konwerter.bintohex2(self.link_layer)
         self.clientFODN = self.konwerter.BIN_to_ascii(self.clientFODN)
         self.createWIAD()



    def createWIAD(self):
        self.WIAD = " transaction_ID: " + str(self.transaction_ID) + " link layer : " + str(
            self.link_layer) + "clientFODN : " + str(self.clientFODN)

    def wyswietl(self):
        print self.WIAD
