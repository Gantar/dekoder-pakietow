import Konwerter

class ICMP(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.type = ""
        self.code = ""
        self.checksum = ""
        self.reserved = ""
        self.number_of_multicast_address = ""
        self.multicast_address = ""

        self.WIAD = ""

    def dlaHex(self, pakiet):
        self.type = pakiet[:2]
        self.code = pakiet[2:4]
        self.checksum = pakiet[4:8]
        self.reserved = pakiet[8:12]
        self.number_of_multicast_address = pakiet[12:16]
        self.multicast_address = pakiet[26:]



        self.type = self.konwerter.HEX_to_dec(self.type)
        self.code = self.konwerter.HEX_to_dec(self.code)
        self.checksum = self.konwerter.HEX_to_dec(self.checksum)
        self.reserved = self.konwerter.HEX_to_dec(self.reserved)
        self.number_of_multicast_address = self.konwerter.HEX_to_dec(self.number_of_multicast_address)
        #self.multicast_address = self.konwerter.HEX_to_ascii(self.multicast_address)


        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.type = pakiet[:8]
        self.code = pakiet[8:16]
        self.checksum = pakiet[16:32]
        self.reserved = pakiet[32:48]
        self.number_of_multicast_address = pakiet[48:64]
        self.multicast_address = pakiet[104:]

        self.type = self.konwerter.BIN_to_dec(self.type)
        self.code = self.konwerter.BIN_to_dec(self.code)
        self.checksum = self.konwerter.BIN_to_dec(self.checksum)
        self.reserved = self.konwerter.BIN_to_dec(self.reserved)
        self.number_of_multicast_address = self.konwerter.BIN_to_dec(self.number_of_multicast_address)
        self.multicast_address = self.konwerter.bintohex2(self.multicast_address)


        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "type: " + str(self.type) + " code: " + str(self.code) + " checksum: " + str(self.checksum) + " reserved: " + str(
            self.reserved) + " number_of_multicast_address: " + str(self.number_of_multicast_address) + " multicast_address: " + str(self.multicast_address)


    def wyswiet(self):
        print self.WIAD

