import Konwerter

class CAT_TP(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.flags = ""
        self.header_Length = ""
        self.source_port = ""
        self.dastination_port = ""
        self.data_length = ""
        self.sequence_number = ""
        self.acknowledgemend_number = ""
        self.window_size = ""
        self.WIAD = ""

    def dlaHex(self, pakiet):
        self.flags = pakiet[:2]
        self.header_Length = pakiet[6:8]
        self.source_port = pakiet[8:12]
        self.dastination_port = pakiet[12:16]
        self.data_length = pakiet[16:20]
        self.sequence_number = pakiet[20:24]
        self.acknowledgemend_number = pakiet[24:28]
        self.window_size = pakiet[28:32]

        #niepotrzebne bo to hex-> self.flags = self.konwerter.HEX_to_ascii(self.flags)
        self.header_Length = self.konwerter.HEX_to_dec(self.header_Length)
        self.source_port = self.konwerter.HEX_to_dec(self.source_port)
        self.dastination_port = self.konwerter.HEX_to_dec(self.dastination_port)
        self.data_length = self.konwerter.HEX_to_dec(self.data_length)
        self.sequence_number = self.konwerter.HEX_to_dec(self.sequence_number)
        self.acknowledgemend_number = self.konwerter.HEX_to_dec(self.acknowledgemend_number)
        self.window_size = self.konwerter.HEX_to_dec(self.window_size)
        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.flags = pakiet[:8]
        self.header_Length = pakiet[24:32]
        self.source_port = pakiet[32:48]
        self.dastination_port = pakiet[48:64]
        self.data_length = pakiet[64:80]
        self.sequence_number = pakiet[80:96]
        self.acknowledgemend_number = pakiet[96:112]
        self.window_size = pakiet[112:128]

        self.flags = self.konwerter.bintohex2(self.flags) #<---podajemy w hex
        self.header_Length = self.konwerter.BIN_to_dec(self.header_Length)
        self.source_port = self.konwerter.BIN_to_dec(self.source_port)
        self.dastination_port = self.konwerter.BIN_to_dec(self.dastination_port)
        self.data_length = self.konwerter.BIN_to_dec(self.data_length)
        self.sequence_number = self.konwerter.BIN_to_dec(self.sequence_number)
        self.acknowledgemend_number = self.konwerter.BIN_to_dec(self.acknowledgemend_number)
        self.window_size = self.konwerter.BIN_to_dec(self.window_size)
        self.createWIAD()

    def createWIAD(self):
        self.WIAD = " flags: " + str(self.flags) + " header_Length: " + str(self.header_Length) + " source_port: " + str(
            self.source_port ) + " dastination_port: " + str(self.dastination_port) + " data_length: " + str(
            self.data_length) + " sequence_number: " + str( self.sequence_number) + " acknowledgemend_number: " + str(
            self.acknowledgemend_number) + " window_size: " + str(self.window_size)

    def wyswietl(self):
        print self.WIAD
