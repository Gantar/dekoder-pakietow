import binascii

class UDP(object):
    def __init__(self):
        self.port_zrodlowy = ""
        self.port_docelowy = ""
        self.dlugosc = ""
        self.suma_kontrolna = ""
        self.dane = ""
        self.WIAD = ""





    def wyswietl(self):
        print self.WIAD

    def createWIAD(self):
        self.WIAD = "Port zrodlowy: " + str(
            self.port_zrodlowy) + " Port docelowy: " + str(
            self.port_docelowy) + " Dlugosc: " + str(
            self.dlugosc) + " Suma kontrolna: " + str(
            self.suma_kontrolna) +" Dane: " + self.dane

    def dlaHex(self, pakiet):
        self.port_zrodlowy = pakiet[:4]
        self.port_docelowy = pakiet[4:8]
        self.dlugosc = pakiet[8:12]
        self.suma_kontrolna = pakiet[12:16]
        self.dane = pakiet[16:]
        self.port_docelowy = self.HEX_to_dec(self.port_docelowy)
        self.port_zrodlowy = self.HEX_to_dec(self.port_zrodlowy)
        self.dlugosc = self.HEX_to_dec(self.dlugosc)
        self.suma_kontrolna = self.HEX_to_dec(self.suma_kontrolna)
        self.dane = self.HEX_to_ascii(self.dane)
        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.port_zrodlowy = pakiet[:16]
        self.port_docelowy = pakiet[16:32]
        self.dlugosc = pakiet[32:48]
        self.suma_kontrolna = pakiet[48:64]
        self.dane = pakiet[64:]
        self.port_docelowy = self.BIN_to_dec(self.port_docelowy)
        self.port_zrodlowy = self.BIN_to_dec(self.port_zrodlowy)
        self.dlugosc = self.BIN_to_dec(self.dlugosc)
        self.suma_kontrolna = self.BIN_to_dec(self.suma_kontrolna)
        self.dane = self.BIN_to_ascii(self.dane)
        self.createWIAD()


    def HEX_to_ascii(self, val):
        return val.decode('hex')

    def HEX_to_dec(self, val):
        return int(val, 16)

    def BIN_to_dec(self, val):
        return int(val, 2)

    def BIN_to_ascii(self, val):
        val = int(str(val), 2)
        return binascii.unhexlify('%x' % val)