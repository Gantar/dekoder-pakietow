# -*- coding: utf-8 -*-
import Konwerter

class ARP(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.hardware_type = ""
        self.protocol_type = ""
        self.hardware_size = ""
        self.protocol_size = ""
        self.opcode = ""
        self.sender_MAC_address = ""
        self.sender_IP_address = ""
        self.target_MAC_address = ""
        self.target_IP_address = ""

        self.WIAD = ""


    def dlaHex(self, pakiet):
        self.hardware_type = pakiet[:4]
        self.protocol_type = pakiet[4:8]
        self.hardware_size = pakiet[8:10]
        self.protocol_size = pakiet[10:12]
        self.opcode = pakiet[12:16]
        self.sender_MAC_address = pakiet[16:28]
        self.sender_IP_address1 = pakiet[28:30]
        self.sender_IP_address2 = pakiet[30:32]
        self.sender_IP_address3 = pakiet[32:34]
        self.sender_IP_address4 = pakiet[34:36]
        #self.sender_IP_address = pakiet[28:36]
        self.target_MAC_address = pakiet[36:48]
        self.target_IP_address1 = pakiet[48:50]
        self.target_IP_address2 = pakiet[50:52]
        self.target_IP_address3 = pakiet[52:54]
        self.target_IP_address4 = pakiet[54:56]
        #self.target_IP_address = pakiet[48:54]

        self.hardware_type = self.konwerter.HEX_to_dec(self.hardware_type)
        self.protocol_type = self.konwerter.HEX_to_dec(self.protocol_type)
        self.hardware_size = self.konwerter.HEX_to_dec(self.hardware_size)
        self.protocol_size = self.konwerter.HEX_to_dec(self.protocol_size)
        self.opcode = self.konwerter.HEX_to_dec(self.opcode)
        #self.sender_MAC_address = self.konwerter.HEX_to_dec(self.sender_MAC_address)
        self.sender_IP_address1 = self.konwerter.HEX_to_dec(self.sender_IP_address1)
        self.sender_IP_address2 = self.konwerter.HEX_to_dec(self.sender_IP_address2)
        self.sender_IP_address3 = self.konwerter.HEX_to_dec(self.sender_IP_address3)
        self.sender_IP_address4 = self.konwerter.HEX_to_dec(self.sender_IP_address4)
        #self.target_MAC_address = self.konwerter.HEX_to_dec(self.target_MAC_address)
        self.target_IP_address1 = self.konwerter.HEX_to_dec(self.target_IP_address1)
        self.target_IP_address2 = self.konwerter.HEX_to_dec(self.target_IP_address2)
        self.target_IP_address3 = self.konwerter.HEX_to_dec(self.target_IP_address3)
        self.target_IP_address4 = self.konwerter.HEX_to_dec(self.target_IP_address4)

        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.hardware_type = pakiet[:16]
        self.protocol_type = pakiet[16:32]
        self.hardware_size = pakiet[32:40]
        self.protocol_size = pakiet[40:48]
        self.opcode = pakiet[48:64]
        self.sender_MAC_address = pakiet[64:112]
        self.sender_IP_address1 = pakiet[112:120]
        self.sender_IP_address2 = pakiet[120:128]
        self.sender_IP_address3 = pakiet[128:136]
        self.sender_IP_address4 = pakiet[136:144]
        #self.sender_IP_address = pakiet[112:144]
        self.target_MAC_address = pakiet[144:192]
        self.target_IP_address1 = pakiet[192:200]
        self.target_IP_address2 = pakiet[200:208]
        self.target_IP_address3 = pakiet[208:216]
        self.target_IP_address4 = pakiet[216:224]
        #self.target_IP_address = pakiet[192:224]


        self.hardware_type = self.konwerter.BIN_to_dec(self.hardware_type)
        self.protocol_type = self.konwerter.BIN_to_dec(self.protocol_type)
        self.hardware_size = self.konwerter.BIN_to_dec(self.hardware_size)
        self.protocol_size = self.konwerter.BIN_to_dec(self.protocol_size)
        self.opcode = self.konwerter.bintohex2(self.opcode)
        self.sender_MAC_address = self.konwerter.bintohex2(self.sender_MAC_address)
        self.sender_IP_address1 = self.konwerter.BIN_to_dec(self.sender_IP_address1)
        self.sender_IP_address2 = self.konwerter.BIN_to_dec(self.sender_IP_address2)
        self.sender_IP_address3 = self.konwerter.BIN_to_dec(self.sender_IP_address3)
        self.sender_IP_address4 = self.konwerter.BIN_to_dec(self.sender_IP_address4)
        self.target_MAC_address = self.konwerter.bintohex2(self.target_MAC_address)
        self.target_IP_address1 = self.konwerter.BIN_to_dec(self.target_IP_address1)
        self.target_IP_address2 = self.konwerter.BIN_to_dec(self.target_IP_address2)
        self.target_IP_address3 = self.konwerter.BIN_to_dec(self.target_IP_address3)
        self.target_IP_address4 = self.konwerter.BIN_to_dec(self.target_IP_address4)

        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "hardware_type: " + str(self.hardware_type) + " protocol_type: " + str(self.protocol_type) + " hardware_size: "+str(
            self.hardware_size) + " protocol_size: " + str(self.protocol_size) + " opcode: " + str(
            self.opcode) + " sender_MAC_address: " + str(
            self.sender_MAC_address) + " sender_IP_address: " + str(
            self.sender_IP_address1)+ "."+ str(self.sender_IP_address2)+ "." + str(self.sender_IP_address3)+ "." + str(self.sender_IP_address4) +" target_MAC_address: " + str(
            self.target_MAC_address) + " target_IP_address:  " + str(self.target_IP_address1)+ "." + str(self.target_IP_address2)+ "." + str(self.target_IP_address3)+ "." + str(self.target_IP_address4)

    def wyswietl(self):
        print self.WIAD

