import Konwerter

class STUN(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.message_type = ""
        self.message_length = ""
        self.message_cookie = ""
        self.message_transaction = ""
        self.WIAD = ""

    def dlaHex(self, pakiet):
        self.message_type = pakiet[:4]
        self.message_length = pakiet[4:8]
        self.message_cookie = pakiet[8:16]
        self.message_transaction = pakiet[16:40]

        #self.message_type = self.konwerter.HEX_to_ascii(self.message_type)
        self.message_length = self.konwerter.HEX_to_dec(self.message_length)
        #self.message_cookie = self.konwerter.HEX_to_ascii(self.message_cookie)
        self.message_transaction = self.konwerter.HEX_to_ascii(self.message_transaction)
        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.message_type = pakiet[:16]
        self.message_length = pakiet[16:32]
        self.message_cookie = pakiet[32:64]
        self.message_transaction = pakiet[64:160]

        self.message_type = self.konwerter.BIN_to_hex(self.message_type)
        self.message_length = self.konwerter.BIN_to_dec(self.message_length)
        self.message_cookie = self.konwerter.BIN_to_hex(self.message_cookie)
        self.message_transaction = self.konwerter.BIN_to_ascii(self.message_transaction)
        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "message_type: " + str(self.message_type) + " message_length: " + str(
            self.message_length) + " message_cookie: " + str(self.message_cookie) + " message_transaction: " + str(
            self.message_transaction)

    def wyswietl(self):
        print self.WIAD