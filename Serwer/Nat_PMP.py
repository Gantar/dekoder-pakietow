import Konwerter

class Nat_PMP(object):
    def __init__(self):
        self.konwerter = Konwerter.Konwerter()
        self.version = ""
        self.opcode = ""
        self.reserved = ""
        self.internal_port = ""
        self.external_port = ""
        self.requested_port_mapping_lifetime = ""
        self.WIAD = ""

    def dlaHex(self, pakiet):
        self.version = pakiet[:2]
        self.opcode = pakiet[2:4]
        self.reserved = pakiet[4:8]
        self.internal_port = pakiet[8:12]
        self.external_port = pakiet[12:16]
        self.requested_port_mapping_lifetime = pakiet[16:]

        self.version = self.konwerter.HEX_to_dec(self.version)
        self.opcode = self.konwerter.HEX_to_dec(self.opcode)
        self.reserved = self.konwerter.HEX_to_dec(self.reserved)
        self.internal_port = self.konwerter.HEX_to_dec(self.reserved)
        self.external_port = self.konwerter.HEX_to_dec(self.external_port)
        self.requested_port_mapping_lifetime = self.konwerter.HEX_to_dec(self.requested_port_mapping_lifetime)
        self.createWIAD()

    def dlaBinary(self, pakiet):
        self.version = pakiet[:8]
        self.opcode = pakiet[8:16]
        self.reserved = pakiet[16:32]
        self.internal_port = pakiet[32:48]
        self.external_port = pakiet[48:64]
        self.requested_port_mapping_lifetime = pakiet[64:]

        self.version = self.konwerter.BIN_to_dec(self.version)
        self.opcode = self.konwerter.BIN_to_dec(self.opcode)
        self.reserved = self.konwerter.BIN_to_dec(self.reserved)
        self.internal_port = self.konwerter.BIN_to_dec(self.reserved)
        self.external_port = self.konwerter.BIN_to_dec(self.external_port)
        self.requested_port_mapping_lifetime = self.konwerter.BIN_to_dec(self.requested_port_mapping_lifetime)
        self.createWIAD()

    def createWIAD(self):
        self.WIAD = "version: " + str(
            self.version) + " opcode: " + str(
            self.opcode) + " reserved: " + str(
            self.reserved) + " internal_port: " + str(
            self.internal_port) + " external_port: " + self.external_port + " requested_port_mapping_lifetime: " + str(
            self.requested_port_mapping_lifetime)

    def wyswiet(self):
        print self.WIAD


